## TP2 DAC
### Auteurs : Muaka Nsilulu Joel && Diakite Aboubakar Siriki

<br/>

Pour ce TP, nous avons créé des `instances` sur `Openstack` à l'aide de `terraform`.

Le code qui permet de mettre en place ces instances se trouve dans le fichier `template.tf`.

Ce code est divisé en six blocs parmis lesquels nous avons :

* Un `terraform` qui permet de définir les providers et fixer les versions.

On retrouve dans ce bloc :

<blockquote>
<ul>
<li><strong>required_version</strong> : prend en compte les versions de terraform à partir de la version 0.13.0 dans notre cas.</li>
<li><strong>required_providers</strong> : déclare les fournisseurs dont terraform a besoin, afin de les installer et les utiliser. Il se compose du nom du fournisseur local, d'un emplacement source du fournisseur qu'on veut utiliser et d'une contrainte de version.</li>
</ul>
</blockquote>

* Deux blocs `variable`qui permettent de créer des variables `user_name` et `password` afin de permettre leur saisie au moment de l'exécution du code.

Ils se composent d'une `description` et d'un `type`.

* Un bloc `provider`qui permet de configurer le fournisseur Openstack.

Il se compose des éléments :

<blockquote>
<ul>
<li><strong>user_name</strong> : le nom d'utilisateur.</li>
<li><strong>auth_url</strong> : l'URL d'authentification du compte utilisateur.</li>
<li><strong>password</strong> : le mot de passe de l'utilisateur.</li>
<li><strong>tenant_id</strong> : l'ID du projet.</li>
</ul>
</blockquote>

Le user_name et le passeword ici, récupère les valeurs saisies à l'éxucution du code grâce aux blocs `variable`.

* Un bloc `resource` permettant de créer une ressource de paire de clés SSH.

Il se compose de :

<blockquote>
<ul>
<li><strong>provider</strong> : correspond au nom du fournisseur.</li>
<li><strong>name</strong> : correspond au mom de la clé SSH à utiliser pour la création.</li>
<li><strong>key_pair</strong> : correspond au chemin vers la clé SSH que nous avons via la commande `ssh-keygen -t rsa`. Elle sera la clé de toutes les instances créées.</li>
</ul>
</blockquote>

* Un autre bloc `resource` permettant de créer les instances.

Il se compose de :

<blockquote>
<ul>
<li><strong>name</strong> : correspond au nom des instances.

Ce nom est à chaque fois suivi d'un indice allant de `0 à count-1` afin de différencier ces instances.</li>
<li><strong>count</strong> : correspond au nombre d'instances à créer.</li>
<li><strong>provider</strong> : correspond au nom du fournisseur.</li>
<li><strong>image_name</strong> : correspond au mom de l'image.</li>
<li><strong>flavor_name</strong> : correspond au nom du type d'instance.</li>
<li><strong>key_pair</strong> : correspond au mom de la clé SSH à utiliser pour la création.</li>
</ul>
</blockquote>

Pour exécuter le code, on a besoin de ces trois commandes :

		terraform init
		terraform plan
		terraform apply

* terraform init : permet d'initialiser l'espace de travail afin de pouvoir télécharger les plugins du provider.

* terraform plan : permet de voir ce qui va être ajouté, créé et supprimé dans l'infrastructure.

* terraform apply : permet d'importer la clé SSH et créer les différentes instances.

L'exécution de cette commande génère un nouveau fichier qui se nomme `terraform.tfstate`. Il permet de stocker l'état de l'infrastructure et de la configuration.

A chaque fois qu'on exécute `terraform applay`, on a l'état actuel de l'infrastructure dans le fichier `terraform.tfstate`. Si ce fichier est déjà présent, l'ancien fichier d'état sera déplacé dans un fichier appelé `terraform.tfstate.backup` et un nouveau fichier `terraform.tfstate` sera créé.
