# TP3 DAC - Ansible
## Auteurs : Muaka Nsilulu Joel && Diakite Aboubakar Siriki

<br/>

Pour ce TP, nous avons travaillé avec `ansible`. Nous avons créé un fichier hosts permettant de contacter nos instances terraform, un grand nombre de fichiers de type playbook avec quelques fichiers utilitaires.

## Contenu

Le TP s'organise en plusieurs fichiers correspondant chacun à une tâche bien précise.

<ul>
<blockquote>
<li>ansible</li>
<ul>
<li>host.ini : fichier inventaire contenant la collection d'hôtes.</li>
</ul>

<li>apache2</li>
<ul>
<li>phpinfo.php : fichier modèle pour la configuration d'une page de test sur le répertoire racine du serveur Web.</li>
<li>playbook.yml : fichier playbook, contenant les tâches à exécuter sur le(s) serveur(s) distant(s) pour installer le server apache2.</li>
<li>vars.yml : fichier variable pour personnaliser les paramètres du playbook.</li>
<li>virtualhost.conf : fichier modèle pour la configuration de l'hôte virtuel Apache.</li>
</ul>

<li>docker</li>
<ul>
<li>default.yml : fichier variable pour personnaliser les paramètres du playbook.</li>
<li>playbook.yml : fichier playbook, contenant les tâches à exécuter sur le(s) serveur(s) distant(s) pour installer docker.</li>
</ul>

<li>ufw</li>
<ul>
<li>main.yml : fichier playbook, contenant les tâches à exécuter sur le(s) serveur(s) distant(s) pour installer le firewall (ufw).</li>
</ul>

</ul>

## Commandes utilitaires :

* Contacter les servers terraform (à partir du repertoire ansible) :
 
		ansible all -m ping -i hosts.ini

* Installer Docker (à partir du repertoire docker) :

		ansible-playbook playbook.yml -i ../ansible/hosts.ini

* Vérifier que les conteneurs docker ont été créés avec succès (en se connectant en ssh à l'un des serveurs distants) :
	
		sudo docker ps -a

* Installer le firwall (à partir du repertoire ufw) :

		ansible-playbook main.yml -i ../ansible/hosts.ini

* Vérifier que les conteneurs docker ont été créés avec succès (en se connectant en ssh à l'un des serveurs distants) :
	
		sudo ufw status

* Installer server web (à partir du repertoire apache2) :

		ansible-playbook main.yml -i ../ansible/hosts.ini
	
	
