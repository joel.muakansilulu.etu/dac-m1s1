## TP1 DAC
### Auteurs : Muaka Nsilulu Joel && Diakite Aboubakar Siriki

<br/>

Pour ce TP, nous avons premièremnt à l'aide de Openstack, créé et téléchargé une paire de clés. Ce fichier est notre clé privé. Il a donc fallu modifier ses permissions afin de s'en servir pour le reste du TP.

La modification des permissions s'effectue à l'aide de la commande suivante :

		chmod 400 <chemin d'accès au fichier clé>
 
Ensuite, nous avons eu besoins de trois instances créées également à partir de Openstack :

		instance1: 172.28.101.126
		instance2: 172.28.100.104
		instance3: 172.28.101.35

Une fois les instances créées, nous nous y sommes connecté en ssh grâce à la commande suivante:

		ssh -i <chemin d'accès au fichier clé> ubuntu@<IP du server>

Une fois connecté à une instance, on se met en position de recevoir un message grâce à la commande netcat :

		nc -l <port>

Puis pour envoyer un message depuis le poste de travail, on tape la commande :

		nc <IP du server> <port>

On peut observer après avoir exécuté les deux commandes que l'échange débute entre les deux machines. Lorsqu'on envoie un message à partir de la première, il apparait automatiquement dans la seconde et vice-versa.

Pour gérer le par-feux, on a utilisé deux commandes :

		sudo ufw enable       // pour activer le par-feux
		sudo ufw disable     // pour désactiver le par-feux

Une fois le par-feux activé au niveau de la machine qui reçoit, toutes les entrées sont bloquées à part le port 22 pour garder la connexion ssh. Aucune autre machine peut entrer en contact avec cette dernière.

On constate alors que tous les messages envoyés à cette machine ne sont pas reçus et tous les messages envoyés par cette machine ne sont pas reçus non plus tant que le par-feux reste activé.
